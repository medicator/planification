#!/usr/bin/env python3

from xml.dom.minidom import parse, parseString

INPUT = "/home/zakcodes/projet/planification/planification.gan"
OUTPUT = "/home/zakcodes/projet/planification/planification.gan"

tree = parse(INPUT)
tasks = tree.getElementsByTagName("task")
for task in tasks:
    name = task.getAttribute("name")
    task.setAttribute('name', name.replace('Copy_', ''))

with open(OUTPUT, "w") as f:
    tree.writexml(f, "\t")
