#!/usr/bin/env python3

from xml.dom.minidom import parse, parseString
from sys import argv

INPUT = argv[1]
OUTPUT = argv[2] if len(argv) >= 3 else argv[1]


def allocationFilter(allocation):
    if allocation.nodeType == allocation.TEXT_NODE:
        return False
    return allocation.getAttribute("resource-id") == "0"


def createAllocation(task_id):
    return parseString(
        f'<allocation task-id="{task_id}" resource-id="0" function="SoftwareDevelopment:2" responsible="true" load="100.0"/>'
    ).documentElement


tree = parse(INPUT)
tasks = tree.getElementsByTagName("task")
tasks_ids = [task.getAttribute("id") for task in tasks]

allocations = tree.getElementsByTagName("allocations")[0]
for n in list(filter(allocationFilter, allocations.childNodes)):
    allocations.childNodes.remove(n)


allocations.childNodes.extend([createAllocation(task_id) for task_id in tasks_ids])

with open(OUTPUT, "w") as f:
    tree.writexml(f, "\t")
