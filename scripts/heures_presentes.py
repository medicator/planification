#!/usr/bin/env python3

from xml.dom.minidom import parse, parseString
import re
from datetime import timedelta

INPUT = "planification.gan"

invalid_count = 0
total_time = timedelta()

timePattern = re.compile("\((\d+)h(\d*)\)")

tree = parse(INPUT)
tasks = tree.getElementsByTagName("task")
for task in tasks:
    if task.getElementsByTagName("task").length == 0:
        name = task.getAttribute("name")
        match = timePattern.search(name)
        if match is None:
            invalid_count += 1
            print(f"La tâche #{task.getAttribute('id')} \"{name}\" devrait avoir une durée valide")
        else:
            hours = int(match[1])
            minutes = 0 if len(match[2]) == 0 else int(match[2])
            total_time += timedelta(hours = hours, minutes = minutes)

        task.setAttribute('name', name.replace('Copy_', ''))

print(f"Tâches invalides: {invalid_count}")

minutes = int(total_time.total_seconds() / 60)
hours = int(minutes / 60)
minutes = minutes % 60
print(f"Temps total: {hours}h{minutes} / {int(27.5 * 14)}h")
