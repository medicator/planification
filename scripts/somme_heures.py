#!/usr/bin/env python3

from xml.dom.minidom import parse, parseString, Node, NodeList
import re
from datetime import timedelta
from sys import argv

INPUT = argv[1]
OUTPUT = argv[2] if len(argv) >= 3 else argv[1]

invalid_count = 0

timePattern = re.compile("\((\d+)h(\d*)\)")
timeParenPattern = re.compile("\(\d+h\d*\)")

def isTask(node: Node) -> bool:
    if node.nodeType == Node.TEXT_NODE:
        return False
    return node.tagName == 'task'

def taskTime(task: Node) -> timedelta:
    name = task.getAttribute("name")
    match = timePattern.search(name)
    if match is None:
        invalid_count += 1
        print(f"La tâche #{task.getAttribute('id')} \"{name}\" devrait avoir une durée valide")
        return timedelta()
    else:
        hours = int(match[1])
        minutes = 0 if len(match[2]) == 0 else int(match[2])
        return timedelta(hours = hours, minutes = minutes)


def formatTime(time: timedelta) -> str:
    minutes = int(time.total_seconds() / 60)
    hours = int(minutes / 60)
    minutes = minutes % 60
    if minutes == 0:
        minutes = ''
    return f"{hours}h{minutes}"


def taskSetTime(task: Node, time: timedelta):
    name = task.getAttribute("name")
    time = formatTime(time)

    match = timeParenPattern.search(name)
    if match is not None:
        name = name.replace(match[0], f"({time})")
    else:
        name += f" ({time})"
    task.setAttribute("name", name)


def process(tasks: NodeList) -> timedelta:
    sum = timedelta()
    for task in tasks:
        childTasks = list(filter(isTask, task.childNodes))
        if len(childTasks) != 0:
            taskTimeDelta = process(childTasks)
            taskSetTime(task, taskTimeDelta)
            sum += taskTimeDelta
        else:
            sum += taskTime(task)
    return sum

# Parse the file
tree = parse(INPUT)
# Get the list of tasks at the first level
rootTasks = list(filter(isTask, tree.getElementsByTagName("tasks")[0].childNodes))
# Process all the tasks and get their total number of hours
total = process(rootTasks[1:])

# Replace the time of the first task
taskSetTime(rootTasks[0], total)

print(f"Temps total: {formatTime(total)} / {int(27.5 * 14)}h")

with open(OUTPUT, "w") as f:
    tree.writexml(f, "\t")
