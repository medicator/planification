#!/bin/bash

IMAGE=${1:-image.png}
GANTT=${2:-planification.gan}

ganttproject -export png \
    -o $(realpath $IMAGE) \
    $(realpath $GANTT)
