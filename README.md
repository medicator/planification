# Planification du projet
Ce dossier contient le gantt de la planification du projet ainsi que des scripts pour:
* Calculer le nombre d'heures que prennent chaque étape
* Générer une image à partir du gantt

Voici l'image la plus récente de la planification:
![Planification](https://gitlab.com/medicator/planification/-/jobs/artifacts/master/raw/image.png?job=gantt-image)
